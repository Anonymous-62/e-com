from django.shortcuts import render
from .models import *
from django.http import JsonResponse
import json
# Create your views here.
def store(request):
    product=Product.objects.all()
    if request.user.is_authenticated:
        customer=request.user.customer
        order,create=Order.objects.get_or_create(customer=customer,complete=False)
        cartItems=order.get_cart_item
    else:
        items=[]
        cartItems=0
    context={'products':product,'cartItems':cartItems}
    return render(request,'store/store.html',context)

def cart(request):
    if request.user.is_authenticated:
        print('authenticate')
        customer=request.user.customer
        order,create=Order.objects.get_or_create(customer=customer,complete=False)
        item=order.orderitem_set.all()
        cartItems=order.get_cart_item
        context={'items':item,'orders':order,'cartItems':cartItems}

    else:
        items=[]
        cartItems=0
        context={
            'orders':'get_cart_item','orders':'get_cart_total','cartItems':cartItems
        }

    return render(request,'store/cart.html',context)

def checkout(request):
    if request.user.is_authenticated:
        print('authenticate')
        customer=request.user.customer
        order,create=Order.objects.get_or_create(customer=customer,complete=False)
        item=order.orderitem_set.all()
        cartItems=order.get_cart_item
        context={'items':item,'orders':order,'cartItems':cartItems}

    else:
        cartItems=0
        context={
            'orders':'get_cart_item','orders':'get_cart_total','cartItems':cartItems
        }

    return render(request,'store/checkout.html',context)

def updateItem(request):
    data=json.loads(request.body)
    productId=data['productId']
    action=data['action']
    customer=request.user.customer
    product=Product.objects.get(id=productId)
    order,create=Order.objects.get_or_create(customer=customer,complete=False)
    orderItem,create=OrderItem.objects.get_or_create(order=order,product=product)
    if action=="add":
        orderItem.quantity=(orderItem.quantity + 1)

    elif action=="remove":
        orderItem.quantity=(orderItem.quantity - 1)

    orderItem.save()
    
    if orderItem.quantity<=0:
        orderItem.delete()
        
    return JsonResponse('data to be added',safe=False)